package eg.edu.alexu.csd.filestructure.redblacktree;



import javax.management.RuntimeErrorException;
import java.util.*;

public class TreeMap<T extends Comparable<T>, V> implements ITreeMap<T, V> {
    private RedBlackTree<T, V> tree;
    TreeMap(){
        tree = new RedBlackTree<>();
    }
    @Override
    public Map.Entry<T, V> ceilingEntry(T key) {
        return new Entry<T, V>(tree.getNode(key, 1));
    }

    @Override
    public T ceilingKey(T key) {
        return ceilingEntry(key).getKey();
    }

    @Override
    public void clear() {
        tree.clear();
    }

    @Override
    public boolean containsKey(T key) {
        return tree.contains(key);
    }

    @Override
    public boolean containsValue(V value) {
        if(value==null) {
            throw new RuntimeErrorException(new Error("Value is null"));
        }
        Iterator<INode<T, V>> iterator = new RBTIterator();
        while(iterator.hasNext()){
            if(iterator.next().getValue().equals(value)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Set<Map.Entry<T, V>> entrySet() {
        Set<Map.Entry<T, V>> hashset= new LinkedHashSet<>();
        Iterator<INode<T, V>> iterator = new RBTIterator();
        while(iterator.hasNext()){
            hashset.add(new Entry<>(iterator.next()));
        }
        return hashset;
    }

    @Override
    public Map.Entry<T, V> firstEntry() {
        Iterator<INode<T, V>> iterator = new RBTIterator();
        if(iterator.hasNext()){
            return new Entry<>(iterator.next());
        }
        return null;
    }

    @Override
    public T firstKey() {
        Map.Entry<T, V> mp = firstEntry();
        if(mp == null){
            return null;
        }
        return mp.getKey();
    }

    @Override
    public Map.Entry<T, V> floorEntry(T key) {
        return new Entry<>(tree.getNode(key, 2));
    }

    @Override
    public T floorKey(T key) {
        return floorEntry(key).getKey();
    }

    @Override
    public V get(T key) {
        return tree.search(key);
    }

    @Override
    public ArrayList<Map.Entry<T, V>> headMap(T toKey) {
        return headMap(toKey, false);
    }

    @Override
    public ArrayList<Map.Entry<T, V>> headMap(T toKey, boolean inclusive) {
        ArrayList<Map.Entry<T, V>> list = new ArrayList<>();
        Iterator<INode<T, V>> iterator = new RBTIterator();
        while(iterator.hasNext()){
            INode<T,V> node = iterator.next();
            if(((inclusive) ? (node.getKey().compareTo(toKey) <= 0) : (node.getKey().compareTo(toKey) < 0)) ){
                list.add(new Entry<>(node));
            }
            break;
        }
        return list;
    }

    @Override
    public Set<T> keySet() {
        Set<T> hashset= new LinkedHashSet<>();
        Iterator<INode<T, V>> iterator = new RBTIterator();
        while(iterator.hasNext()){
            hashset.add(iterator.next().getKey());
        }
        return hashset;
    }

    @Override
    public Map.Entry<T, V> lastEntry() {
        INode<T, V> node = tree.getRoot();
        if(node.isNull()){
            return null;
        }
        while(!node.getRightChild().isNull()){
            node = node.getRightChild();
        }
        return new Entry<T, V>(node);
    }

    @Override
    public T lastKey() {
        Map.Entry<T, V> mp = lastEntry();
        if(mp == null){
            return null;
        }
        return mp.getKey();
    }

    @Override
    public Map.Entry<T, V> pollFirstEntry() {
        Map.Entry<T, V> mp = firstEntry();
        if(mp == null){
            return null;
        }
        remove(mp.getKey());
        return mp;
    }

    @Override
    public Map.Entry<T, V> pollLastEntry() {
        Map.Entry<T, V> mp = lastEntry();
        if(mp == null){
            return null;
        }
        remove(mp.getKey());
        return mp;
    }

    @Override
    public void put(T key, V value) {
        tree.insert(key, value);
    }

    @Override
    public void putAll(Map<T, V> map) {
        if(map == null) {
            throw new RuntimeErrorException(new Error("Map is empty"));
        }
        for(Map.Entry<T, V> entry : map.entrySet()){
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public boolean remove(T key) {
        return tree.delete(key);
    }

    @Override
    public int size() {
        return tree.getSize();
    }

    @Override
    public Collection<V> values() {
        List<V> val = new ArrayList<V>();
        Iterator<INode<T, V>> iterator = new RBTIterator();
        while(iterator.hasNext()){
            val.add(iterator.next().getValue());
        }
        return val;
    }
    static class Entry<T extends Comparable<T>, V> implements Map.Entry<T, V>{
        T key;
        V value;
        Entry(INode<T, V> node){
            this.key = node.getKey();
            this.value = node.getValue();
        }
        @Override
        public T getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V v) {
            this.value = v;
            return v;
        }
        @Override
        public boolean equals(Object obj){
            if(obj == null){
                return false;
            }
            Map.Entry<T,V> et;
            try {
                et = (Map.Entry<T, V>) obj;
            }catch(Exception e){
                return false;
            }
            return et.getKey().equals(this.getKey()) && et.getValue().equals(this.getValue());
        }
    }
    class RBTIterator implements Iterator<INode<T, V>> {
        Stack<INode<T, V>> stk = new Stack<>();

        RBTIterator(){
            setupNext(tree.getRoot());
        }
        void setupNext(INode<T, V> node){
            while(!node.isNull()){
                stk.push(node);
                node = node.getLeftChild();
            }
        }
        @Override
        public boolean hasNext() {
            return !stk.isEmpty();
        }

        @Override
        public INode<T, V> next() {
            INode<T,V> node = stk.peek();
            stk.pop();
            if(!node.getRightChild().isNull()){
                setupNext(node.getRightChild());
            }
            return node;
        }
    }
}
