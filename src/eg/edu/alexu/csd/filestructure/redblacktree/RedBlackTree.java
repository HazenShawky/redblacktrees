package eg.edu.alexu.csd.filestructure.redblacktree;

import javax.management.RuntimeErrorException;
import java.util.concurrent.atomic.AtomicBoolean;

public class RedBlackTree<T extends Comparable<T>, V> implements IRedBlackTree<T, V>{
    private INode<T, V> root;
    private INode<T, V> nullNode = new Node(null, null, false, null, null, null);
    private boolean flag = false;
    private int size = 0;
    RedBlackTree(){
        this.root = createNullNode(nullNode);
    }
    INode<T, V> createNullNode(INode<T, V> parent){
        return new Node<T,V>(null, null, false, parent, null, null);
    }
    @Override
    public INode<T, V> getRoot() {
        return root;
    }

    @Override
    public boolean isEmpty() {
        return root.isNull();
    }

    @Override
    public void clear() {
        root = null;
        root = createNullNode(null);
        size = 0;
    }
    public int getSize(){
        return size;
    }

    @Override
    public V search(T key) {
        return getNode(key,0).getValue();
    }


    @Override
    public boolean contains(T key) {
        return !getNode(key,0).isNull();
    }

    public INode<T, V> getNode(T key, int flag){
        if(key == null){
            throw new RuntimeErrorException(new Error("Key is null"));
        }
        if(root.isNull()){
            return root;
        }
        INode<T, V> curNode = root, successor = root, predecessor = root;
        int cmp = 0;
        while(!curNode.isNull()){
            cmp = curNode.getKey().compareTo(key);
            if(cmp == 0){
                return curNode;
            }
            else if(cmp > 0){
                successor = curNode;
                curNode = curNode.getLeftChild();
            }else{
                predecessor = curNode;
                curNode = curNode.getRightChild();
            }
        }
        if(flag==0){
            return curNode;
        }else if(flag==1){
            return successor;
        }else{
            return predecessor;
        }
    }
    public INode<T, V> getGrandParent(INode<T, V> node){
        return ((node.getParent().isNull()) ? node.getParent() : node.getParent().getParent());
    }
    public INode<T, V> getSibling(INode<T, V> node){
        if(node.getParent().isNull())
            throw new RuntimeErrorException(new Error("Cannot get sibling"));
        INode<T, V> parent = node.getParent();
        if(parent.getLeftChild() == node){
            return parent.getRightChild();
        }else{
            return parent.getLeftChild();
        }
    }
    public INode<T, V> getUncle(INode<T, V> node){
        if(node.getParent().isNull())
            throw new RuntimeErrorException(new Error("Cannot get Uncle"));
        return getSibling(node.getParent());
    }
    void RotateLeft(INode<T, V> node){
        INode<T, V> right = node.getRightChild();
        INode<T, V> par = node.getParent();
        if(right.isNull()){
            throw new RuntimeErrorException(new Error("Right node is a null node, Can't rotate left"));
        }
        node.setRightChild(right.getLeftChild());
        right.setLeftChild(node);
        node.setParent(right);
        node.getRightChild().setParent(node);
        if(!par.isNull()){
            if(node == par.getLeftChild()){
                par.setLeftChild(right);
            }else{
                par.setRightChild(right);
            }
        }
        right.setParent(par);
    }

    void RotateRight(INode<T, V> node){
        INode<T, V> left = node.getLeftChild();
        INode<T, V> parent = node.getParent();
        if(left.isNull()){
            throw new RuntimeErrorException(new Error("Left node is a null node, Can't rotate Right"));
        }
        node.setLeftChild(left.getRightChild());
        left.setRightChild(node);
        node.setParent(left);
        node.getLeftChild().setParent(node);
        if(!parent.isNull()){
            if(node == parent.getRightChild()){
                parent.setRightChild(left);
            }else{
                parent.setLeftChild(left);
            }
        }
        left.setParent(parent);
    }
    @Override
    public void insert(T key, V value) {
        flag = false;
        INode<T, V> node = insertNode(key, value);
        if(!flag){
            ++size;
            insertFixTree(node);
        }
        root = node;
        while(!root.getParent().isNull()){
            root = root.getParent();
        }
        return;
    }
    void insertFixTree(INode<T, V> node){
        if(node.getParent().isNull()){
            node.setColor(false);
        }else if(!node.getParent().getColor()){
        }else if(!getUncle(node).isNull() && getUncle(node).getColor()){
            node.getParent().setColor(false);
            getUncle(node).setColor(false);
            getGrandParent(node).setColor(true);
            insertFixTree(getGrandParent(node));
        }else{
            INode<T, V> par = node.getParent();
            INode<T, V> grand = getGrandParent(node);
            if(node == par.getRightChild() && par == grand.getLeftChild()){
                RotateLeft(par);
                node = node.getLeftChild();
            }else if(node == par.getLeftChild() && par == grand.getRightChild()){
                RotateRight(par);
                node = node.getRightChild();
            }
            par = node.getParent();
            grand = getGrandParent(node);
            if(node == par.getLeftChild()){
                RotateRight(grand);
            }else{
                RotateLeft(grand);
            }
            par.setColor(false);
            grand.setColor(true);
        }
    }
    public INode<T, V> insertNode(T key, V value){
        if(key == null || value == null){
            throw new RuntimeErrorException(new Error("Key is null"));
        }
        if(root.isNull()){
            root = new Node(key, value, false, nullNode, null, null);
            nullNode.setLeftChild(root);
            root.setLeftChild(createNullNode(root));
            root.setRightChild(createNullNode(root));
            return root;
        }
        INode<T, V> curNode = root;
        int cmp = 0;
        while(!curNode.isNull()){
            cmp = curNode.getKey().compareTo(key);
            if(cmp == 0){
                curNode.setValue(value);
                flag = true;
                return curNode;
            }
            else if(cmp > 0){
                if(curNode.getLeftChild().isNull()){
                    break;
                }
                curNode = curNode.getLeftChild();

            }else{
                if(curNode.getRightChild().isNull()){
                    break;
                }
                curNode = curNode.getRightChild();
            }
        }
        INode<T,V> node = new Node(key, value, true, curNode, null, null);
        if(cmp > 0){
            curNode.setLeftChild(node);
            node.setLeftChild(createNullNode(node));
            node.setRightChild(createNullNode(node));
        }else{
            curNode.setRightChild(node);
            node.setLeftChild(createNullNode(node));
            node.setRightChild(createNullNode(node));
        }
        return node;
    }
    private INode<T, V> toDelete(INode<T,V> node){
        if(!node.getLeftChild().isNull()){
            return getSuccessor(node);
        }else if(!node.getRightChild().isNull()){
            return getPredecessor(node);
        }
        return node;
    }
    public INode<T, V> getSuccessor(INode<T, V> node){
        node = node.getLeftChild();
        while(!node.getRightChild().isNull()){
            node = node.getRightChild();
        }
        return node;
    }
    public INode<T, V> getPredecessor(INode<T, V> node){
        node = node.getRightChild();
        while(!node.getLeftChild().isNull()){
            node = node.getLeftChild();
        }
        return node;
    }
    @Override
    public boolean delete(T key) {
        INode<T, V> node = getNode(key, 0);
        if(node.isNull()){
            return false;
        }
        --size;
        INode<T, V> todelete = toDelete(node);
        node.setValue(todelete.getValue());
        node.setKey(todelete.getKey());
        INode<T, V> child = ((todelete.getRightChild().isNull()) ? todelete.getLeftChild() : todelete.getRightChild());
        child.setParent(todelete.getParent());
        if(todelete == todelete.getParent().getLeftChild()){
            todelete.getParent().setLeftChild(child);
        }else{
            todelete.getParent().setRightChild(child);
        }
        if(todelete.getColor() == false){
            if(child.getColor() == true){
                child.setColor(false);
            }else{
                if(!child.getParent().isNull()){
                    deleteCase1(child);
                }
            }
        }
        root = child;
        while(!root.getParent().isNull()){
            root = root.getParent();
        }
        return true;
    }



    private void deleteCase1(INode<T, V> node){
        INode<T,V> sibling = getSibling(node);

        if(sibling.getColor() == true){
            node.getParent().setColor(true);
            sibling.setColor(false);
            if(node==node.getParent().getLeftChild()){
                RotateLeft(node.getParent());
            }else{
                RotateRight(node.getParent());
            }
        }
        deleteCase2(node);
    }


    private void deleteCase2(INode<T,V> node){
        INode<T,V> sibling = getSibling(node);
        if(node.getParent().getColor() == false && sibling.getColor()==false && sibling.getLeftChild().getColor()==false && sibling.getRightChild().getColor()==false){
            sibling.setColor(true);
            if(!getGrandParent(node).isNull())
                deleteCase1(node.getParent());
        }else{
            deleteCase3(node);
        }
    }


    private void deleteCase3(INode<T,V> node){
        INode<T,V> sibling = getSibling(node);
        if(node.getParent().getColor() == true && sibling.getColor()==false && sibling.getLeftChild().getColor()==false && sibling.getRightChild().getColor()==false){
            sibling.setColor(true);
            node.getParent().setColor(false);
        }else{
            deleteCase4(node);
        }
    }


    private void deleteCase4(INode<T, V> node){
        INode<T,V> sibling = getSibling(node);
        if ((node == node.getParent().getLeftChild()) && (sibling.getRightChild().getColor() == false) &&
                (sibling.getLeftChild().getColor() == true)){
            sibling.setColor(true);
            sibling.getLeftChild().setColor(false);
            RotateRight(sibling);
        }else if((node == node.getParent().getRightChild()) && (sibling.getRightChild().getColor() == true) &&
                (sibling.getLeftChild().getColor() == false)){
            sibling.setColor(true);
            sibling.getRightChild().setColor(false);
            RotateLeft(sibling);
        }
        deleteCase5(node);
    }
    private void deleteCase5(INode<T, V> node){
        INode<T,V> sibling = getSibling(node);
        sibling.setColor(node.getParent().getColor());
        node.getParent().setColor(false);
        if(node == node.getParent().getLeftChild()){
            sibling.getRightChild().setColor(false);
            RotateLeft(node.getParent());
        }else{
            sibling.getLeftChild().setColor(false);
            RotateRight(node.getParent());
        }

    }
}
